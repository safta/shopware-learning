<?php declare(strict_types=1);

namespace ItdelightShopwareLearning\Service\ScheduledTask;

use Psr\Log\LoggerInterface;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTaskHandler;

class NotificationTaskHandler extends ScheduledTaskHandler
{
    private LoggerInterface $logger;

    public function __construct(
        EntityRepositoryInterface $repository,
        LoggerInterface $logger
    ) {
        parent::__construct($repository);
        $this->logger = $logger;
    }

    public static function getHandledMessages(): iterable
    {
        return [NotificationTask::class];
    }

    public function run(): void
    {
        $this->logger->notice('Notification from cron');
    }
}