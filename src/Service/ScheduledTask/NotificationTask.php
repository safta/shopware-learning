<?php declare(strict_types=1);

namespace ItdelightShopwareLearning\Service\ScheduledTask;

use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTask;
use Shopware\Core\System\SystemConfig\SystemConfigService;

class NotificationTask extends ScheduledTask
{

    public static function getTaskName(): string
    {
        return 'itdelight.send_notification';
    }

    public static function getDefaultInterval(): int
    {
        return 120;
    }
}