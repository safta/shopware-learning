<?php declare(strict_types=1);

namespace ItdelightShopwareLearning\Calculator;

class SumCalculator
{
    public function sumTwoDigits(int $a, int $b): int
    {
        return $a + $b;
    }
}