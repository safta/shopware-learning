<?php declare(strict_types=1);

namespace ItdelightShopwareLearning\Test\Service\Calculator;

use ItdelightShopwareLearning\Calculator\SumCalculator;
use PHPUnit\Framework\TestCase;

class SumCalculatorTest extends TestCase
{
    private SumCalculator $sumCalculator;

    public function dataProvideTwoDigit(): array
    {
        return [
            [2, 2]
        ];
    }

    protected function setUp(): void
    {
        $this->sumCalculator = new SumCalculator();
    }

    /**
     * @dataProvider dataProvideTwoDigit
     */
    public function testSumTwoDigits(int $a, int $b)
    {
        $actualResult = $this->sumCalculator->sumTwoDigits($a, $b);
        $this->assertEquals(4,$actualResult);
    }
}