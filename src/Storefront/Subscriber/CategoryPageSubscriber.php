<?php declare(strict_types=1);

namespace ItdelightShopwareLearning\Storefront\Subscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CategoryPageSubscriber implements EventSubscriberInterface
{

    public static function getSubscribedEvents(): array
    {
        return ['frontend.navigation.page.request' => 'onCategoryPageLoad'];
    }

    public function onCategoryPageLoad($event)
    {
        /* @todo write some logic here */
    }
}