<?php declare(strict_types=1);

namespace ItdelightShopwareLearning\Storefront\Subscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CheckoutFinishPageSubscriber implements EventSubscriberInterface
{

    public static function getSubscribedEvents(): array
    {
        return ['frontend.checkout.finish.page.request' => 'onCheckoutFinishOrder'];
    }

    public function onCheckoutFinishOrder($event)
    {
        /* @todo write some logic here */
    }
}