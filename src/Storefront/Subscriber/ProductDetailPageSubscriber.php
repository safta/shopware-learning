<?php declare(strict_types=1);

namespace ItdelightShopwareLearning\Storefront\Subscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ProductDetailPageSubscriber implements EventSubscriberInterface
{

    public static function getSubscribedEvents(): array
    {
        return ['frontend.detail.page.request' => 'onProductPageLoaded'];
    }

    public function onProductPageLoaded($event)
    {
        /* @todo write some logic here */
    }
}