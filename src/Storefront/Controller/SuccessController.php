<?php declare(strict_types=1);

namespace ItdelightShopwareLearning\Storefront\Controller;

use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(defaults={"_routeScope"={"storefront"}})
 */
class SuccessController extends StorefrontController
{
    /**
     * @Route("/get-success", name="frontend.success.success", defaults={"XmlHttpRequest"=true}, methods={"GET"})
     */
    public function getSuccess(): JsonResponse
    {
        return new JsonResponse(['success' => true]);
    }
}
