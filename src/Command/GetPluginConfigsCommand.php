<?php declare(strict_types=1);

namespace ItdelightShopwareLearning\Command;

use Shopware\Core\System\SystemConfig\SystemConfigService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class GetPluginConfigsCommand extends Command
{
    const FIELD_NAME = 'field_name';
    const FIELD_NOT_EXIST = 'The configuration field does not exist';

    /**
     * @var string
     */
    protected static $defaultName = 'itdelight:get:plugin:config';

    /**
     * @var SystemConfigService
     */
    private SystemConfigService $systemConfigService;

    /**
     * @param SystemConfigService $systemConfigService
     */
    public function __construct(SystemConfigService $systemConfigService)
    {
        $this->systemConfigService = $systemConfigService;
        parent::__construct();
    }

    /** @inerhitDoc */
    protected function configure()
    {
        $this->addArgument(self::FIELD_NAME, InputArgument::REQUIRED,
            'The name of input fields in plugin configuration');
        $this->setDescription('Command provides getting plugin configuration fields values by name');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $fieldName = $input->getArgument(self::FIELD_NAME);
        $pluginConfig = $this->systemConfigService->get('ItdelightShopwareLearning.config');
        $result = array_key_exists($fieldName, $pluginConfig) ? $pluginConfig[$fieldName] : self::FIELD_NOT_EXIST;
        $output->writeln($result);
        return 0;
    }

}